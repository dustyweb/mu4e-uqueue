;;; mu4e-unreadqueue.el -- extension to mu4e for treating unread
;;;   maildirs as a queue
;;
;; Copyright (C) 2012 Christopher Allan Webber

;; This file is not part of GNU Emacs.
;;
;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.

; To use this, simply load this file and run:
;   M-x mu4e-uqueue
;
; ...this will open up a buffer that lists all the maildirs you have mail in 

(require 'cl)
(require 'subr-x)

;; (defun mu4e-uqueue~find-result-count (query)
;;   "Find the number of results that match a QUERY find with mu"
;;   (string-to-number
;;    (gnus-strip-whitespace
;;     (shell-command-to-string
;;      (format "%s find %s flag:unread 2>/dev/null | wc -l"
;;              mu4e-mu-binary query)))))


;; (defun mu4e-uqueue~unread-for-maildir (maildir)
;;   (mu4e-uqueue~find-result-count
;;    (replace-regexp-in-string " " "\\\\ "
;;                              (format "\"maildir:%s\"" maildir))))


;; (defvar mu4e-uqueue~maildir-cache nil)

;; (defun mu4e-uqueue~get-maildirs-cached ()
;;   (or mu4e-uqueue~maildir-cache
;;       (let ((maildirs (mu4e-get-maildirs)))
;;         (setq mu4e-uqueue~maildir-cache maildirs)
;;         maildirs)))

;; (defun mu4e-uqueue~unread-all-maildirs ()
;;   (mapcar
;;    (lambda (maildir)
;;      (cons maildir (mu4e-uqueue~unread-for-maildir maildir)))
;;    (mu4e-uqueue~get-maildirs-cached)))

;; (defvar mu4e-uqueue-directory
;;   (file-name-directory load-file-name))

;; (defun mu4e-uqueue~unread-all-maildirs ()
;;   (let ((shell-out (shell-command-to-string
;;                     (concat "guix environment --ad-hoc guile@2.2 mu -- guile -e main "
;;                             (file-name-as-directory mu4e-uqueue-directory)
;;                             "maildir-counts.scm ~/Maildir/ 2>/dev/null"))))
;;     (read shell-out)))

(defun mu4e-uqueue~scan-directory (base-dir)
  (let ((subdirs (directory-files base-dir nil directory-files-no-dot-files-regexp)))
    (if (seq-contains-p subdirs "new")
        ;; We've reached a maildir, let's count the number in "new"
        (let* ((new-dir (concat (file-name-as-directory base-dir) "new"))
               (num-new-files
                (length
                 (directory-files new-dir nil
                                  directory-files-no-dot-files-regexp))))
          num-new-files)
      ;; Otherwise let's cons on each subdurectory with this 
      (mapcar (lambda (subdir)
                (cons subdir
                      (mu4e-uqueue~scan-directory
                       (concat (file-name-as-directory base-dir) subdir))))
              subdirs))))

(defun mu4e-uqueue~collapse-directories (dirtree)
  (letrec ((collapsed '())
           (collapse-subtree
            (lambda (subtree parent-path)
              (let ((maildir-name
                     (if parent-path
                         (concat (file-name-as-directory parent-path)
                                 (car subtree))
                       (car subtree))))
                (if (numberp (cdr subtree))
                  (setq collapsed (cons (cons maildir-name (cdr subtree)) collapsed))
                  (mapc (lambda (subtree) (funcall collapse-subtree subtree maildir-name))
                        (cdr subtree)))))))
    (mapc (lambda (subtree) (funcall collapse-subtree subtree nil))
          dirtree)
    (reverse collapsed)))

(defun mu4e-uqueue~unread-all-maildirs ()
  (mu4e-uqueue~collapse-directories (mu4e-uqueue~scan-directory "~/Maildir")))

(defun mu4e-uqueue~nonempty-unread-all-maildirs ()
  (seq-remove (lambda (md-c)
                (eql (cdr md-c) 0))
              (mu4e-uqueue~unread-all-maildirs)))

(defun mu4e-uqueue~unread-hashtable (unread)
  (let ((ht (make-hash-table :test 'equal)))
    (dolist (md-c unread)
      (let ((maildir (car md-c))
            (count (cdr md-c)))
        (puthash maildir count ht)))
    ht))


(defun mu4e-uqueue~insert-subgroup (subgroup-name subgroup-maildirs mailcount-ht)
  ;; First we insert the subgroup
  (insert (propertize (format "%s:" subgroup-name)
                      'font-lock-face 'mu4e-title-face))
  (add-text-properties (line-beginning-position)
                       (line-end-position)
                       `(mu4e-uqueue-maildirs ,subgroup-maildirs))
  (insert "\n")
  ;; Then we insert each maildir that's part of the subgroup
  (dolist (maildir subgroup-maildirs)
    (let ((count (gethash maildir mailcount-ht)))
      (when (and count (not (eql count 0)))
        (insert "  ")
        (insert (propertize maildir 'font-lock-face
                            'font-lock-variable-name-face))
        (insert (format ": %s" count))
        (add-text-properties
         (line-beginning-position) (line-end-position)
         `(mu4e-uqueue-maildir ,maildir))
        (insert "\n")))))

(defun mu4e-uqueue-update-queue ()
  (interactive)
  (let ((inhibit-read-only t))
    (erase-buffer)
    (let* ((mailcounts (mu4e-uqueue~nonempty-unread-all-maildirs))
           (unread-maildirs (mapcar 'car mailcounts))
           (mailcount-ht (mu4e-uqueue~unread-hashtable mailcounts))
           (subgroups-maildirs (apply 'append (mapcar 'cdr mu4e-uqueue-subgroups)))
           (unsorted-unread (cl-set-difference unread-maildirs
                                               subgroups-maildirs
                                               :test 'equal)))
      ;; Insert all the main groups
      (dolist (sg-mds mu4e-uqueue-subgroups)
        (let ((subgroup-name (car sg-mds))
              (subgroup-maildirs (cdr sg-mds)))
          ;; Only show subgroup if it has mail
          (when (cl-block 'FOUND-IT
                  (dolist (maildir subgroup-maildirs)
                    (let ((count (gethash maildir mailcount-ht)))
                      (and count (not (eql count 0))
                           (cl-return-from 'FOUND-IT t))))
                  nil)
            (mu4e-uqueue~insert-subgroup subgroup-name subgroup-maildirs mailcount-ht))))
      ;; Insert the unsorted special group
      (and unsorted-unread
           (mu4e-uqueue~insert-subgroup "*Unsorted*" unsorted-unread mailcount-ht))))
  (beginning-of-buffer))

(defun mu4e-uqueue-update-queue-old ()
  (interactive)
  (let ((inhibit-read-only t))
    (erase-buffer)
    (dolist (mailcount (mu4e-uqueue~unread-all-maildirs))
      (let ((maildir (car mailcount))
            (count (cdr mailcount)))
        (if (> count 0)
            (progn
              (insert "  ")
              (insert (propertize maildir 'font-lock-face 'font-lock-variable-name-face))
              (insert (format ": %s" count))
              (add-text-properties
               (line-beginning-position) (line-end-position)
               `(mu4e-uqueue-maildir ,maildir))
              (insert "\n")))))
    (beginning-of-buffer)))


(defun mu4e-uqueue-visit-query ()
  (interactive)
  (let ((maildir (get-text-property (point) 'mu4e-uqueue-maildir))
        (maildirs (get-text-property (point) 'mu4e-uqueue-maildirs)))
    (cond
     (maildir
      (mu4e-search (format "\"maildir:/%s\" flag:unread" maildir) nil))
     (maildirs
      (let ((joined-maildirs
             (string-join (mapcar (lambda (maildir)
                                    (format "\"maildir:/%s\"" maildir))
                                  maildirs)
                          " OR ")))
        (mu4e-search (format "(%s) AND flag:unread" joined-maildirs) nil))))))


(defun mu4e-uqueue ()
  (interactive)
  (switch-to-buffer (get-buffer-create "*mu4e-uqueue*"))
  (mu4e-uqueue-mode)
  (mu4e-uqueue-update-queue))


(define-derived-mode mu4e-uqueue-mode special-mode "mu4e-uqueue"
  "A queue of mail directories or queries to go through in mu4e")


(defvar mu4e-uqueue-mode-map (make-sparse-keymap))

(defvar mu4e-uqueue-subgroups nil
  "A mapping of subgroups to maildirs that fit under them.

Eg '((\"My-subgroup\" \"MSGMaildir1\" \"MSGMaildir2\") ...)")


(define-key mu4e-uqueue-mode-map "n" 'next-line)
(define-key mu4e-uqueue-mode-map "p" 'previous-line)
(define-key mu4e-uqueue-mode-map (kbd "RET") 'mu4e-uqueue-visit-query)
(define-key mu4e-uqueue-mode-map "g" 'mu4e-uqueue-update-queue)
(define-key mu4e-uqueue-mode-map "U" 'mu4e-update-mail-and-index)
(define-key mu4e-uqueue-mode-map "u" 'mu4e-update-index)

