#!/usr/bin/env guile \
-e main -s
!#


(use-modules (mu)
             (mu stats)
             (ice-9 ftw)
             (ice-9 match))

(unless (mu:initialized?)
  (mu:initialize))

(define (get-maildirs maildirs-path)
  (filter (match-lambda
            [(or "." "..") #f]
            [_ #t])
          (scandir maildirs-path)))

(define (maildir-unread-count maildir)
  (mu:count (format #f "\"maildir:/~a\" flag:unread" maildir)))

(define (maildir-count maildir)
  (mu:count (format #f "\"maildir:/~a\"" maildir)))

(define (get-maildir-counts maildirs-path)
  (map (lambda (maildir)
         (cons maildir (maildir-unread-count maildir)))
       (get-maildirs maildirs-path)))

(define (main args)
  (match args
    [(_ maildirs-path)
     (write (get-maildir-counts maildirs-path))]))

